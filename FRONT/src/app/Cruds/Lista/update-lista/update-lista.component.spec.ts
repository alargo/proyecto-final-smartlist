import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateListaComponent } from './update-lista.component';

describe('UpdateListaComponent', () => {
  let component: UpdateListaComponent;
  let fixture: ComponentFixture<UpdateListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
