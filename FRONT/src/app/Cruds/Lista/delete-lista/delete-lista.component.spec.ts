import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteListaComponent } from './delete-lista.component';

describe('DeleteListaComponent', () => {
  let component: DeleteListaComponent;
  let fixture: ComponentFixture<DeleteListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
