import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbrirListaComponent } from './abrir-lista.component';

describe('AbrirListaComponent', () => {
  let component: AbrirListaComponent;
  let fixture: ComponentFixture<AbrirListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbrirListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbrirListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
