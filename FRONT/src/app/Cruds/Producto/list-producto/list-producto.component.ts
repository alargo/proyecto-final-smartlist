import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ListaProductoService } from 'src/app/service/listaProducto.service';
import { UsuarioListaService } from 'src/app/service/usuarioLista.service';
import { Producto } from '../../../model/producto';
import { ProductoService } from '../../../service/producto.service';
import { TokenStorageService } from '../../../service/token-storage.service';
import { Lista } from '../../../model/lista';
import { ListaProducto } from 'src/app/model/listaProducto';
import { ListaService } from 'src/app/service/lista.service';

@Component({
  selector: 'app-list-producto',
  templateUrl: './list-producto.component.html',
  styleUrls: ['./dashboard.css']
})
export class ListProductoComponent implements OnInit {
  //productos: Observable<Producto[]>;
  productos: Producto[] = new Array();
  arrayLista: Lista[] = new Array();
  producto: Producto = new Producto();
  listaProducto: ListaProducto = new ListaProducto();
  campoBusqueda: string;
  codigoProducto: number;
  constructor(private productoService: ProductoService, private listaService: ListaService,
    private router: Router,private token: TokenStorageService,
    private listaProductoService: ListaProductoService, private usuarioListaService: UsuarioListaService) {}

  ngOnInit() {


    if(this.token.getUser().roles == "ROLE_ADMIN"){
     $('#editar').show();
     $('#eliminar').show();
    }
    this.reloadData();
    this.mostrarSmartList();
  }


  reloadData() {
    //this.productos = this.productoService.getProductoList();
    console.log(this.productos)
    this.productoService.getProductoList()
      .subscribe(
        data => {
          //console.log(data);
          this.productos = data;
        },
        error => console.log(error));
  }

  deleteProducto(cod_producto: number) {
    this.productoService.deleteProducto(cod_producto)
      .subscribe(
        data => {
          //console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  productoDetails(cod_producto: number){
    this.router.navigate(['details', cod_producto]);
  }

  updateProducto(cod_producto: number){
    this.router.navigate(['update', cod_producto]);
  }

  newProductoLista(producto, lista): void {
    this.listaProducto = new ListaProducto();
    this.listaProducto.lista = lista;
    this.listaProducto.producto  = producto;
  }

  mostrarSmartList(){
    this.usuarioListaService.getUsuarioListaList().subscribe(
      (data) => {
        data.forEach((element) => {
          if (element.usuario.id == this.token.getUser().correo) {
            this.arrayLista.push(element.lista);
          }
        });
        //console.log(this.arrayLista);
      },
      (error) => console.log(error)
    );
  }


  buscar(){
    if($('#campoBusqueda').val().toString() != ""){
      this.campoBusqueda = $('#campoBusqueda').val().toString().toLowerCase().split(' ').join('')

    }else{
      this.campoBusqueda = null
    }

    this.producto.nombre

  }


  agregarProductoSmartList(nombreProducto: string){
    var codigoLista = parseInt($('#smartlists option:selected').val().toString());
    var nombreLista = $('#smartlists option:selected').text().toString();

    this.listaProducto = new ListaProducto();
    this.productoService.getProductoList().subscribe(
      (data)=>{
       // console.log(data);
        data.forEach((element) => {
          if (element.nombre == nombreProducto) {
            this.producto = element;

            this.newProductoLista(element.cod_Producto,codigoLista)
            console.log(element.cod_Producto)
            this.listaProductoService.createProducto(this.listaProducto)
                    .subscribe(
                (data) => {
                  alert("Se ha agregado "+nombreProducto+" a la SmartList: "+nombreLista)
                  console.log(data);

                },

                (error) => console.log(error),


              );
            }
          });

      },
      (error)=>console.log(error)
    );






}
  }


