import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AbrirListaComponent } from './Cruds/Lista/abrir-lista/abrir-lista.component';
import { CreateListaComponent } from './Cruds/Lista/create-lista/create-lista.component';
import { ListListaComponent } from './Cruds/Lista/list-lista/list-lista.component';
import { CreateProductoComponent } from './Cruds/Producto/create-producto/create-producto.component';
import { ListProductoComponent } from './Cruds/Producto/list-producto/list-producto.component';
import { BuscarUsuarioComponent } from './Cruds/Usuario/buscar-usuario/buscar-usuario.component';
import { CrearUsuarioComponent } from './Cruds/Usuario/crear-usuario/crear-usuario.component';
import { CuentaUsuarioComponent } from './Cruds/Usuario/cuenta-usuario/cuenta-usuario.component';
import { ListUsuarioComponent } from './Cruds/Usuario/list-usuario/list-usuario.component';
import { InicioComponent } from './Home/inicio/inicio.component';
import { E404Component } from './Info/e404/e404.component';

const routes: Routes = [

  {path: 'inicio', component:InicioComponent},
  {path: 'login', component:BuscarUsuarioComponent},
  {path: 'registro', component:CrearUsuarioComponent},
  {path: 'misSmartList', component:ListListaComponent},
  {path: 'crearSmartList', component:CreateListaComponent},
  {path: 'listarProducto', component:ListProductoComponent},
  {path: 'cuenta', component:CuentaUsuarioComponent},
  {path: 'abrirSmartList/:codigo_lista', component:AbrirListaComponent},
  {path: 'crearProducto', component:CreateProductoComponent},
  {path: 'listarUsuario', component:ListUsuarioComponent},
  {path: '**', redirectTo: '/404'},
  {path: '404', component:E404Component},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
