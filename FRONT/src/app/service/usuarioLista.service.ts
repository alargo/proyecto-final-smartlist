import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UsuarioListaService {

  private baseUrl = '/api/usuarioLista';

  constructor(private http: HttpClient) { }

  getUsuarioLista(cod_usuarioLista: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${cod_usuarioLista}`);
  }

  createUsuarioLista(usuarioLista: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, usuarioLista);
  }

  updateUsuarioLista(cod_usuarioLista: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${cod_usuarioLista}`, value);
  }

  deleteUsuarioLista(cod_usuarioLista: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${cod_usuarioLista}`, { responseType: 'text' });
  }

  getUsuarioListaList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
