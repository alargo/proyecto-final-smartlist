import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from '../model/usuario';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  private baseUrl = '/api/usuario';

  constructor(private http: HttpClient) {}

  getUsuario(correo: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${correo}`);
  }

  createUsuario(usuario: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, usuario);
  }

  updateUsuario(correo: string, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${correo}`, value);
  }

  deleteUsuario(correo: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${correo}`, {
      responseType: 'text',
    });
  }

  getUsuarioList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
