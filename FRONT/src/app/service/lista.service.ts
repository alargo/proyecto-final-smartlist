import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ListaService {

  private baseUrl = '/api/lista';

  constructor(private http: HttpClient) { }

  getLista(codigo_lista: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${codigo_lista}`);
  }

  createLista(lista: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, lista);
  }

  updateLista(codigo_lista: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${codigo_lista}`, value);
  }

  deleteLista(codigo_lista: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${codigo_lista}`, { responseType: 'text' });
  }

  getListaList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
