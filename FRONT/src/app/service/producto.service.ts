import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProductoService {

  private baseUrl = '/api/producto';

  constructor(private http: HttpClient) { }

  getProducto(cod_producto: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${cod_producto}`);
  }

  createProducto(producto: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, producto);
  }

  updateProducto(cod_producto: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${cod_producto}`, value);
  }

  deleteProducto(cod_producto: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${cod_producto}`, { responseType: 'text' });
  }

  getProductoList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
