import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./cover.css']
})
export class PrincipalComponent implements OnInit {

  constructor(private router: Router) { }



  ngOnInit(): void {
    var rutaActual = window.location.pathname;


    if(rutaActual == '/inicio'){
      $( '#inicioClick' ).addClass( 'active' );
    }else if(rutaActual == '/login'){
      $( '#loginClick' ).addClass( 'active' );
    }else{
      $( '#registroClick' ).addClass( 'active' );
    }

  }

}
