import { Lista } from './lista';
import { Usuario } from './usuario';

export class UsuarioLista {
  id: number;
  usuario: string;
  lista: number;
  rol: string;
}
