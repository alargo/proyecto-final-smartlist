DROP SCHEMA IF EXISTS `SMARTLIST`;
CREATE SCHEMA  IF NOT EXISTS `SMARTLIST`;
USE `SMARTLIST`;


CREATE TABLE `lista` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `creador` varchar(45) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `nombre_lista` varchar(45) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

CREATE TABLE `rol` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `producto` (
  `cod_producto` INT NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `precio` double NOT NULL,
  `categoria` enum('Lacteos','Carne y pescado','Legumbres, Frutos Secos','Verduras y hortalizas','Frutas','Grasas y aceites','Cereales, derivados y dulces','Bebidas') NOT NULL,
  `codigoQR` varchar(150) DEFAULT NULL,
  `cantidad` int NOT NULL,
  PRIMARY KEY (`cod_producto`)
) ;

CREATE TABLE `lista_tiene_producto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cod_lista` INT NOT NULL,
  `cod_producto` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `CodLista` FOREIGN KEY (`cod_lista`) REFERENCES `lista` (`codigo`),
  CONSTRAINT `CodProducto` FOREIGN KEY (`cod_producto`) REFERENCES `producto` (`cod_producto`)
) ;

CREATE TABLE `usuario` (
  `correo` varchar(100) NOT NULL,
  `nom_apels` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `date_nacimiento` date DEFAULT NULL,
  PRIMARY KEY (`correo`)
);
CREATE TABLE `usuario_participa_lista` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `correo_usuario` varchar(100) NOT NULL,
  `codigo_lista` INT NOT NULL,
  `rol` enum('Administrador','Editor','Vista') DEFAULT 'Editor',
  PRIMARY KEY (`id`),
  CONSTRAINT `CodigoLista` FOREIGN KEY (`codigo_lista`) REFERENCES `lista` (`codigo`),
  CONSTRAINT `CorreoUsuario` FOREIGN KEY (`correo_usuario`) REFERENCES `usuario` (`correo`)
);
CREATE TABLE `usuario_rol` (
  `user_id` varchar(100) NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`),
  CONSTRAINT `fkUserId` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`correo`),
  CONSTRAINT `fkRoleId` FOREIGN KEY (`role_id`) REFERENCES `rol` (`id`)
);
INSERT INTO `rol` (name) VALUES ('ROLE_USER'),('ROLE_ADMIN');
INSERT INTO `lista` (creador,fecha_creacion,nombre_lista,descripcion) VALUES ('Marc Infante','2020-08-14','Barbacoa','Lista de la compra'),
('Jose Manuel','2020-08-14','Comida domingo1','Lista amigos'),('Roberto Garcia','2020-08-14','Lista de la compra','Compra de casa'),
('Jose Manuel','2020-08-14','Comida domingo2','Lista amigos'),('Jose Manuel','2020-08-14','Comida domingo3','Lista amigos'),
('Jose Manuel','2020-08-14','Comida domingo4','Lista amigos'),('Jose Manuel','2020-08-14','Comida domingo','Lista amigos'),
('Jose Manuel','2020-08-14','Comida domingo6','Lista amigos'),('Jose Manuel','2020-08-14','Comida domingo5','Lista amigos');
INSERT INTO `producto` (nombre,precio,categoria,codigoQR,cantidad) VALUES ('Coca-Cola',1,'Bebidas',NULL,50),('Lays Al punto de Sal',1.2,'Verduras y hortalizas',NULL,150),('Manzana',1.2,'Frutas',NULL,150),('Carne de cordero',1.2,'Carne y pescado',NULL,150),('Chocapic',1.2,'Cereales, derivados y dulces',NULL,150),('Agua',1.2,'Bebidas',NULL,150);
INSERT INTO `usuario` (correo,nom_apels,password,username,date_nacimiento) VALUES ('prueba@prueba.com','Roberto Garcia','$2a$10$7U/TJ9gA.kDWzuHZWaCjfuJl/izDujC6fQdeg4vuQwcq.UYbetCm6','Robertito','2000-09-18'),('prueba2@prueba.es','David Hidalgo','$2a$10$hhdYS4nZ5krUdxCPnnJbAuQI2flr2LG4Y.FeJDhFOQHU.LXh2lwWK','DHidal','1995-12-12');
INSERT INTO `lista_tiene_producto` (cod_lista,cod_producto) VALUES ('1','1'),('2','2'),('3','2'),('4','2'),('5','2'),('6','2'),('7','2'),('8','2'),('9','2');
INSERT INTO `usuario_participa_lista` (correo_usuario,codigo_lista,rol) VALUES ('prueba@prueba.com','1','Administrador'),('prueba2@prueba.es','2','Editor'),
('prueba2@prueba.es','3','Editor'),('prueba2@prueba.es','4','Editor'),('prueba@prueba.com','5','Editor'),('prueba@prueba.com','6','Editor'),('prueba@prueba.com','7','Editor')
,('prueba@prueba.com','8','Editor'),('prueba@prueba.com','9','Editor');
INSERT INTO `usuario_rol` (user_id,role_id) VALUES ('prueba@prueba.com','1'),('prueba2@prueba.es','2');
