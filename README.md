Equipo: Marc Infante
        Daniel Jiménez
        Alejandro Largo
        Juan José Carretero 

Temática: Organización, Compras

Descripción: Permitir a un usuario realizar una lista de la compra de un supermercado juntamente con otros usuarios. Se permitirá añadir o quitar productos de la lista manualmente o mediante su código de barras (inicialmente código QR).

Si un producto no esta disponible se habilitará una opción para que te geolocalice el supermercado mas cercano.
