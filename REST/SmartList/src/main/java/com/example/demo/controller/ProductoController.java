package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Producto;
import com.example.demo.service.ProductoServiceImpl;



@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
public class ProductoController {
	
	@Autowired
	ProductoServiceImpl productoService;
	
	@GetMapping("/producto")
	public List<Producto> listarProductos(){
		return productoService.listarProductos();
	}
	
	@PostMapping("/producto")
	public Producto salvarProducto(@RequestBody Producto producto) {
		
		return productoService.guardarProducto(producto);
	}
	
	@GetMapping("/producto/{cod_producto}")
	public Producto ProductoXCod_Producto(@PathVariable(name="cod_producto") int cod_producto) {
		
		Producto producto_xCod_Producto= new Producto();
		
		producto_xCod_Producto = productoService.productoXCod_Producto(cod_producto);
		
		System.out.println("producto XCod_Producto:  " + producto_xCod_Producto);
		
		return producto_xCod_Producto;
	}
	
	@PutMapping("/producto/{cod_producto}")
	public Producto actualizarProducto(@PathVariable(name="cod_producto")int cod_producto,@RequestBody Producto producto) {
		
		Producto producto_seleccionado= new Producto();
		Producto producto_actualizado= new Producto();
		
		producto_seleccionado= productoService.productoXCod_Producto(cod_producto);
		
		producto_seleccionado.setNombre(producto.getNombre());
		producto_seleccionado.setPrecio(producto.getPrecio());
		producto_seleccionado.setCategoria(producto.getCategoria());
		producto_seleccionado.setCodigoQR(producto.getCodigoQR());
		producto_seleccionado.setCantidad(producto.getCantidad());
		
		producto_actualizado = productoService.actualizarProducto(producto);
		
		System.out.println("El producto actualizado es: "+ producto_actualizado);
		
		return producto_actualizado;
	}
	
	@DeleteMapping("/producto/{cod_producto}")
	public void eliminarProducto(@PathVariable(name="cod_producto")int cod_producto) {
		productoService.eliminarProducto(cod_producto);
	}

}
