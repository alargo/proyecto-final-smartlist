package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.UsuarioParticipaLista;

public interface IUsuarioParticipaListaDao extends JpaRepository<UsuarioParticipaLista, Integer> {

}
