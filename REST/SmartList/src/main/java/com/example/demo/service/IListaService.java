package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Lista;

public interface IListaService {

	//Metodos del CRUD
			public List<Lista> listarLista(); 
			
			public Lista guardarLista(Lista lista);	
			
			public Lista ListaXcodigo(int codigo); 
			
			public Lista actualizarLista(Lista lista); 
			
			public void eliminarLista(int codigo);
}
