package com.example.demo.dto;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



@Entity
@Table(name="lista")

public class Lista {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="codigo")
	private int id;
	@Column(name="creador")
	private String creador;
	@Column(name="fecha_creacion")
	private Date fecha_creacion;
	@Column(name="nombre_lista")
	private String nombre_lista;
	@Column(name="descripcion")
	private String descripcion;
	
	
	@OneToMany(mappedBy="lista", cascade = CascadeType.ALL)
	private List<ListaTieneProducto> idLista;

	@OneToMany
	@JoinColumn(name="id")
	private List<UsuarioParticipaLista> idUsuario;

	public Lista(int codigo, String creador, Date fecha_creacion, String nombre_lista, String descripcion,
			List<ListaTieneProducto> idLista, List<UsuarioParticipaLista> idUsuario) {
		this.id = codigo;
		this.creador = creador;
		this.fecha_creacion = fecha_creacion;
		this.nombre_lista = nombre_lista;
		this.descripcion = descripcion;
		this.idLista = idLista;
		this.idUsuario = idUsuario;
	}
	
	public Lista() {

	}

	public int getCodigo() {
		return id;
	}

	public void setCodigo(int codigo) {
		this.id = codigo;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public String getNombre_lista() {
		return nombre_lista;
	}

	public void setNombre_lista(String nombre_lista) {
		this.nombre_lista = nombre_lista;
	}


	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<ListaTieneProducto> getIdLista() {
		return idLista;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ListaTieneProducto")
	public void setIdLista(List<ListaTieneProducto> idLista) {
		this.idLista = idLista;
	}

	public List<UsuarioParticipaLista> getIdUsuario() {
		return idUsuario;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "UsuarioParticipaLista")
	public void setIdUsuario(List<UsuarioParticipaLista> idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
	@Override
	public String toString() {
		return "Lista [codigo=" + id + ", creador=" + creador + ", fecha_creacion=" + fecha_creacion
				+ ", nombre_lista=" + nombre_lista + ", tipo=" + descripcion + "]";
	}

	
	
	
	
	
	
	
	
	
	
	

}
