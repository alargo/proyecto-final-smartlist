package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.Lista;

public interface IListaDao extends JpaRepository<Lista, Integer> {

}
