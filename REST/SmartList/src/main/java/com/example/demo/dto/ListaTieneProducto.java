package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="lista_tiene_producto")
public class ListaTieneProducto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="cod_lista")
	private Lista lista;
	
	@ManyToOne
	@JoinColumn(name="cod_producto")
	private Producto producto;
	
	@JsonProperty("lista")
	private void unpackNested(Integer codigo) {
	    this.lista = new Lista();
	    lista.setCodigo(codigo);
	}
	
	@JsonProperty("producto")
	private void unpackNested(int codigoP) {
	    this.producto = new Producto();
	    this.producto.setCod_Producto(codigoP);
	}

	/**
	 * 
	 */
	public ListaTieneProducto() {
	}

	/**
	 * @param id
	 * @param lista
	 * @param producto
	 */
	public ListaTieneProducto(int id, Lista lista, Producto producto) {
		super();
		this.id = id;
		this.lista = lista;
		this.producto = producto;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lista
	 */
	public Lista getLista() {
		return lista;
	}

	/**
	 * @param lista the lista to set
	 */
	public void setLista(Lista lista) {
		this.lista = lista;
	}

	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "ListaTieneProducto [id=" + id + ", lista=" + lista + ", producto=" + producto + "]";
	}
	
	
	
	
	

}
