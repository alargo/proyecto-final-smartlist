package com.example.demo.payload.request;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.*;
 
public class SignupRequest {
	
    @NotBlank
    @Size(max = 50)
    @Email
    private String correo;
    
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;
    
    @NotBlank
    @Size(min = 3, max = 50)
    private String nom_apels;
    
    @NotBlank
    private Date date_nacimiento;

    
    private String role;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNom_apels() {
		return nom_apels;
	}

	public void setNom_apels(String nom_apels) {
		this.nom_apels = nom_apels;
	}

	public Date getDate_nacimiento() {
		return date_nacimiento;
	}

	public void setDate_nacimiento(Date date_nacimiento) {
		this.date_nacimiento = date_nacimiento;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
  
	
}
