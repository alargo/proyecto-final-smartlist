package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.ListaTieneProducto;

public interface IListaTieneProductoService {
	
	//Metodos del CRUD
			public List<ListaTieneProducto> listarListaTieneProducto();
			
			public ListaTieneProducto guardarListaTieneProducto(ListaTieneProducto listaTieneProducto);	
			
			public ListaTieneProducto listaTieneProductoXID(int id);
			
			public ListaTieneProducto actualizarListaTieneProducto(ListaTieneProducto listaTieneProducto); 
			
			public void eliminarListaTieneProducto(int id);

	

}
