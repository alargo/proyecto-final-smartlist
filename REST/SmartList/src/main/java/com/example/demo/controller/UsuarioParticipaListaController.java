package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UsuarioParticipaLista;
import com.example.demo.service.IUsuarioParticipaListaService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})

public class UsuarioParticipaListaController {
	
	@Autowired
	IUsuarioParticipaListaService usuarioParticipaListaService;
	
	@GetMapping("/usuarioLista")
	public List<UsuarioParticipaLista> listarUsuarioParticipaLista(){
		return usuarioParticipaListaService.listarUsuarioParticipaLista();
	}
	
	
	@PostMapping("/usuarioLista")
	public UsuarioParticipaLista salvarUsuarioParticipaLista(@RequestBody UsuarioParticipaLista usuarioParticipaLista) {
		
		return usuarioParticipaListaService.guardarUsuarioParticipaLista(usuarioParticipaLista);
	}
	
	
	@GetMapping("/usuarioLista/{id}")
	public UsuarioParticipaLista usuarioParticipaListaXID(@PathVariable(name="id") int id) {
		
		UsuarioParticipaLista usuarioParticipaLista_xid= new UsuarioParticipaLista();
		
		usuarioParticipaLista_xid=usuarioParticipaListaService.usuarioParticipaListaXID(id);
		
		System.out.println("Usuarios relacionados con listas XID: "+usuarioParticipaLista_xid);
		
		return usuarioParticipaLista_xid;
	}
	
	@PutMapping("/usuarioLista/{id}")
	public UsuarioParticipaLista actualizarUsuarioParticipaLista(@PathVariable(name="id")int id,@RequestBody UsuarioParticipaLista usuarioParticipaLista) {
		
		UsuarioParticipaLista usuarioParticipaLista_seleccionado= new UsuarioParticipaLista();
		UsuarioParticipaLista usuarioParticipaLista_actualizado= new UsuarioParticipaLista();
		
		usuarioParticipaLista_seleccionado= usuarioParticipaListaService.usuarioParticipaListaXID(id);
		
		usuarioParticipaLista_seleccionado.setUsuario(usuarioParticipaLista.getUsuario());
		usuarioParticipaLista_seleccionado.setLista(usuarioParticipaLista.getLista());
		usuarioParticipaLista_seleccionado.setRol(usuarioParticipaLista.getRol());
		
		usuarioParticipaLista_actualizado = usuarioParticipaListaService.actualizarUsuarioParticipaLista(usuarioParticipaLista_seleccionado);
		
		System.out.println("La relacion usuario-lista actualizada es: "+ usuarioParticipaLista_actualizado);
		
		return usuarioParticipaLista_actualizado;
	}
	
	@DeleteMapping("/usuarioLista/{id}")
	public void eleiminarUsuarioParticipaLista(@PathVariable(name="id")int id) {
		usuarioParticipaListaService.eliminarUsuarioParticipaLista(id);
	}

}
