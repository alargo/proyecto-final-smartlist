package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="producto")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cod_producto")
	private int cod_Producto;
	@Column(name="nombre")
	private String nombre;
	@Column(name="precio")
	private double precio;
	@Column(name="categoria")
	private String categoria;
	@Column(name="codigoQR")
	private String codigoQR;
	@Column(name="cantidad")
	private int cantidad;
	
	
	@OneToMany
	@JoinColumn(name="id")
	private List<ListaTieneProducto> id;
	
	

	public Producto() {

	}
	

	public Producto(int cod_Producto, String nombre, double precio, String categoria, String codigoQR, int cantidad,
			List<ListaTieneProducto> id) {
		this.cod_Producto = cod_Producto;
		this.nombre = nombre;
		this.precio = precio;
		this.categoria = categoria;
		this.codigoQR = codigoQR;
		this.cantidad = cantidad;
		this.id = id;
	}


	public int getCod_Producto() {
		return cod_Producto;
	}


	public void setCod_Producto(int cod_Producto) {
		this.cod_Producto = cod_Producto;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public String getCategoria() {
		return categoria;
	}


	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public String getCodigoQR() {
		return codigoQR;
	}


	public void setCodigoQR(String codigoQR) {
		this.codigoQR = codigoQR;
	}


	public int getCantidad() {
		return cantidad;
	}


	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	public List<ListaTieneProducto> getId() {
		return id;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ListaTieneProducto")
	public void setId(List<ListaTieneProducto> id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return "Producto [cod_Producto=" + cod_Producto + ", nombre=" + nombre + ", precio=" + precio + ", categoria="
				+ categoria + ", codigoQR=" + codigoQR + ", cantidad=" + cantidad + "]";
	}
	
	
}
