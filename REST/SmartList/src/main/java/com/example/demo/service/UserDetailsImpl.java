package com.example.demo.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.demo.dto.Usuario;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	
	private String nom_apels;
	
	private Date date_nacimiento;

	private String username;

	private String correo;

	private String password;
	
	private Collection<? extends GrantedAuthority> authorities;


	

	/**
	 * @param nom_apels
	 * @param date_nacimiento
	 * @param username
	 * @param correo
	 * @param password
	 * @param authorities
	 */
	public UserDetailsImpl(String nom_apels, Date date_nacimiento, String username, String correo, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.nom_apels = nom_apels;
		this.date_nacimiento = date_nacimiento;
		this.username = username;
		this.correo = correo;
		this.password = password;
		this.authorities = authorities;
	}


	public static UserDetailsImpl build(Usuario user) {
		List<GrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getName().name()))
				.collect(Collectors.toList());

		return new UserDetailsImpl(
				user.getNom_apels(), 
				user.getDate_nacimiento(),
				user.getUsername(),
				user.getId(),
				user.getPassword(),
				authorities);
	}

	
	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }


	

	public String getCorreo() {
		return correo;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}
	
	

	public Date getDate_nacimiento() {
		return date_nacimiento;
	}
	
	

	public String getNom_apels() {
		return nom_apels;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(correo, user.getCorreo());
	}
}
