package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Producto;



public interface IProductoService {

	//Metodos del CRUD
	public List<Producto> listarProductos(); 
	
	public Producto guardarProducto(Producto producto);	
	
	public Producto productoXCod_Producto(int producto); 
	
	public Producto actualizarProducto(Producto producto); 
	
	public void eliminarProducto(int producto);
}
