package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.IListaDao;
import com.example.demo.dto.Lista;

@Service
public class ListaServiceImpl implements IListaService {

	@Autowired
	IListaDao iListaDao;
	
	@Override
	public List<Lista> listarLista() {
		return iListaDao.findAll();
	}

	@Override
	public Lista guardarLista(Lista lista) {
		return iListaDao.save(lista);
	}

	@Override
	public Lista ListaXcodigo(int codigo) {
		return iListaDao.findById(codigo).get();
	}

	@Override
	public Lista actualizarLista(Lista lista) {
		return iListaDao.save(lista);
	}

	@Override
	public void eliminarLista(int codigo) {
		iListaDao.deleteById(codigo);
		
	}

	
}
