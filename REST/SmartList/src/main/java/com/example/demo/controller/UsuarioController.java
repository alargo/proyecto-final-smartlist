package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Producto;
import com.example.demo.dto.Usuario;
import com.example.demo.service.UsuarioServiceImpl;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})

public class UsuarioController {
	
	@Autowired
	UsuarioServiceImpl usuarioService;
	
	@GetMapping("/usuario")
	public List<Usuario> listarUsuarios(){
		return usuarioService.listarUsuarios();
	}
	
	@PostMapping("/usuario")
	public Usuario salvarUsuario(@RequestBody Usuario usuario) {
		return usuarioService.guardarUsuario(usuario);
	}
	
	@GetMapping("/usuario/{correo}")
	public Usuario usarioXID(@PathVariable(name="correo") String correo) {
		
		Usuario usuarioXID= new Usuario();
		
		usuarioXID = usuarioService.usuarioXID(correo);
		
		System.out.println("Usuario por id:  " + usuarioXID);
		
		return usuarioXID;
	}
	
	@PutMapping("/usuario/{correo}")
	public Usuario actualizarUsuario(@PathVariable(name="correo")String id,@RequestBody Usuario usuario) {
		
		Usuario usuario_seleccionado= new Usuario();
		Usuario usuario_actualizado= new Usuario();
		
		usuario_seleccionado= usuarioService.usuarioXID(id);
		
		usuario_seleccionado.setNom_apels(usuario.getNom_apels());
		
		usuario_seleccionado.setPassword(usuario.getPassword());
		
		usuario_seleccionado.setUsername(usuario.getUsername());
		
		usuario_actualizado = usuarioService.actualizarUsuario(usuario);
		
		System.out.println("El usuario actualizado es: "+ usuario_actualizado);
		
		return usuario_actualizado;
	}
	
	@DeleteMapping("/usuario/{correo}")
	public void eliminarUsuario(@PathVariable(name="correo")String id) {
		usuarioService.eliminarUsuario(id);
	}
}
