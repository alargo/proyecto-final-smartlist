package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IUsuarioDao;
import com.example.demo.dto.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService{
	
	@Autowired
	IUsuarioDao usuarioDao;
	
	@Override
	public List<Usuario> listarUsuarios() {
		return usuarioDao.findAll();
	}

	@Override
	public Usuario guardarUsuario(Usuario usuario) {
		return usuarioDao.save(usuario);
	}

	@Override
	public Usuario usuarioXID(String id) {
		return usuarioDao.findById(id).get();
	}

	@Override
	public Usuario actualizarUsuario(Usuario usuario) {
		return usuarioDao.save(usuario);
	}

	@Override
	public void eliminarUsuario(String id) {
		usuarioDao.deleteById(id);
	}

}
