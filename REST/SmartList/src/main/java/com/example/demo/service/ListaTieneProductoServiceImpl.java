package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IListaTieneProductoDao;
import com.example.demo.dto.ListaTieneProducto;

@Service
public class ListaTieneProductoServiceImpl implements IListaTieneProductoService{

	@Autowired
	IListaTieneProductoDao listaTieneProductoDAO;
	
	@Override
	public List<ListaTieneProducto> listarListaTieneProducto() {
		return listaTieneProductoDAO.findAll();
	}

	@Override
	public ListaTieneProducto guardarListaTieneProducto(ListaTieneProducto listaTieneProducto) {
		return listaTieneProductoDAO.save(listaTieneProducto);
	}

	@Override
	public ListaTieneProducto listaTieneProductoXID(int id) {
		return listaTieneProductoDAO.findById(id).get();
	}

	@Override
	public ListaTieneProducto actualizarListaTieneProducto(ListaTieneProducto listaTieneProducto) {
		return listaTieneProductoDAO.save(listaTieneProducto);
	}

	@Override
	public void eliminarListaTieneProducto(int id) {
		listaTieneProductoDAO.deleteById(id);
		
	}

}
