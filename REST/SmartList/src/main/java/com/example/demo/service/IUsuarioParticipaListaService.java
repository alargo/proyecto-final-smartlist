package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.UsuarioParticipaLista;

public interface IUsuarioParticipaListaService {

	//Metodos del CRUD
	public List<UsuarioParticipaLista> listarUsuarioParticipaLista();
	
	public UsuarioParticipaLista guardarUsuarioParticipaLista(UsuarioParticipaLista usuarioParticipaLista);	
	
	public UsuarioParticipaLista usuarioParticipaListaXID(int id);
	
	public UsuarioParticipaLista actualizarUsuarioParticipaLista(UsuarioParticipaLista usuarioParticipaLista); 
	
	public void eliminarUsuarioParticipaLista(int id);
}
